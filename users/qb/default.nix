{ config, pkgs, lib, ... }:
let
  pubkeys = import ../pubkeys.nix;
in
{
  users.extraUsers.nina = {
    isNormalUser = true;
    initialPassword = "changepw";
  };
  users.extraUsers.tom = {
    isNormalUser = true;
    initialPassword = "changepw";
  };
  users.extraUsers.qb = {
    isNormalUser = true;
    initialPassword = "changepw";
  };
}
