{ config, lib, ... }:
let
  pubkeys = import ../pubkeys.nix;
in
lib.mkIf config.myrealm.layer.users
{
  users.users.root = {
    openssh.authorizedKeys.keys = lib.mkDefault [
      pubkeys.leo_p
    ];
    #initialPassword = "changepw";
  };
}
