{ lib, config, ... }:
let
  pubkeys = import ../pubkeys.nix;
in
lib.mkIf config.myrealm.layer.users
{
  users.users.leo = {
    isNormalUser = true;
    extraGroups = lib.mkDefault [ "wheel" "networkmanager" "audio" "video" "lp" "dialout" "libvirtd" "kvm" "nm-openvpn" "lxd" "disk" "scanner" "usb" "docker" "uucp" "users" "docker"];
    openssh.authorizedKeys.keys = lib.mkDefault [
      pubkeys.leo_p
    ];
    initialPassword = "changepw";
  };
}
