{ lib, pkgs, ... }:
let
  unstable = import <nixpkgs-unstable> {};

  basic-packages = with unstable; [
    #bash
    #coreutils
    gitMinimal
  ];

  zephyr-python-packages = unstable.python3.withPackages(ps: with ps; [
    pyelftools
    pyyaml
    pykwalify
    canopen
    packaging
    progress
    psutil
    anytree
    intelhex
    west
    cryptography
    intelhex
    click
    cbor
  ]);

  zephyr-packages = with unstable; [
    gcc-arm-embedded
    cmake
    ninja
    gperf
    python3
    ccache
    dtc
    (zephyr-python-packages)
  ];

  nix-host-script = {packages, script}: pkgs.writeScript "setup-container" ''
          mkdir -p -m 0755 /nix/var/log/nix/drvs
          mkdir -p -m 0755 /nix/var/nix/gcroots
          mkdir -p -m 0755 /nix/var/nix/profiles
          mkdir -p -m 0755 /nix/var/nix/temproots
          mkdir -p -m 0755 /nix/var/nix/userpool
          mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
          mkdir -p -m 1777 /nix/var/nix/profiles/per-user
          mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
          mkdir -p -m 0700 "$HOME/.nix-defexpr"
          . ${pkgs.nix}/etc/profile.d/nix.sh
          ${pkgs.nix}/bin/nix-env -i ${lib.strings.concatStringsSep " " (with pkgs; [ nix cacert git openssh ])}
          ${pkgs.nix}/bin/nix-channel --add https://nixos.org/channels/nixpkgs-unstable
          ${pkgs.nix}/bin/nix-channel --update nixpkgs
          ${pkgs.nix}/bin/nix-env -i ${lib.strings.concatStringsSep " " (with pkgs; packages)}
          ${script}
        '';

  nix-host-env = {
    ENV = "/etc/profile";
    USER = "root";
    NIX_REMOTE = "daemon";
    PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
    NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
  };

  nix-host-vol = [
    "/nix/store:/nix/store:ro"
    "/nix/var/nix/db:/nix/var/nix/db:ro"
    "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
  ];

  zephyr-prebuild-script = pkgs.writeScript "setup-workspace" ''
    west init
    west update
  '';

  alpineFromDockerHub = pkgs.dockerTools.pullImage {
    imageName = "alpine";
    imageDigest = "sha256:e7d88de73db3d3fd9b2d63aa7f447a10fd0220b7cbf39803c803f2af9ba256b3";
    sha256 = "sha256-Mep+6rwp04QqOB9OPHn5wBQEZmBVojzJOL1OjzLjd1w=";
    finalImageName = "alpine";
    finalImageTag = "latest";
  };

  zephyrDockerImage = pkgs.dockerTools.buildImage {
      fromImage = alpineFromDockerHub;
      name = "zephyr-ci";
      tag = "latest";
      contents = basic-packages ++ zephyr-packages;
  };
in
{
  virtualisation.oci-containers = {
    backend = "docker";
    containers = {
      zephyr-ci = {
        image = "zephyr-ci";
        imageFile = zephyrDockerImage;
        autoStart = false;
      };
    };
  };

  services.gitlab-runner = {
    enable = true;
    services = {
      ggm-test = {
        executor = "shell";
        registrationConfigFile = /etc/nixos/secrets/ggm-runner.env;
        environmentVariables = { } // nix-host-env;
        tagList = [ "nix" "test" "deploy" "ggm" ];
      };

      zephyr = {
        executor = "docker";
        registrationConfigFile = /etc/nixos/secrets/zephyr-runner.env;
        dockerImage = "zephyr-ci";
        registrationFlags = [
          "--docker-pull-policy if-not-present"
        ];
        dockerVolumes = [
          "/usbdata/gitlab-runner/builds:/builds:rw"
        ];
        environmentVariables = {
          ZEPHYR_TOOLCHAIN_VARIANT = "gnuarmemb";
          GNUARMEMB_TOOLCHAIN_PATH = "/";
        };
        tagList = [ "zephyr" "build" ];
      };

      blog = {
        executor = "docker";
        #buildsDir = /usb/gitlab-runner/builds;
        #dockerPrivileged = true;
        registrationConfigFile = /etc/nixos/secrets/blog-runner.env;
        dockerImage = "alpine";
        preBuildScript = nix-host-script {
          packages = with pkgs; [ exiftool fd zola imagemagick ];
          script = "";
        };
        dockerVolumes = [ ] ++ nix-host-vol;
        environmentVariables = { } // nix-host-env;
        dockerDisableCache = true;
        #dockerDisableCache = false;
        tagList = [ "nix" "build" "zola" ];
      };
    };
  };
}
