{
  # all
  imports = [
    ./docker.nix
    #./gitlab-runner.nix
    #./libvirt.nix
    #./lxd.nix
    ./nanomq.nix
    #./phpdev.nix
    ./phppgadmin.nix
    #./postgresql.nix
  ];
}
