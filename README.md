## myrealm

### mandatory
#### myrealm/infra/hosts and/or layers/domain/*/default.nix
* `layer.users.enable = true` <- needed for deployment etc.
* `<?>.storage.enable`
* `<?>.storage.mutableDir`
* `<?>.storage.staticDir`

### recommended
* `profile.services = "ssh-only"` -> set in layer defaults?

### optional
* `profile.enable`
* `layer.enable`
