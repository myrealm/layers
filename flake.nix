{
  description = "modules";

  inputs = {
    storage = { url = "gitlab:myrealm/storage/main"; };
    #storage = { url = "git+ssh://gitea@np.lan/living-systems/storage?ref=main"; };
    nixpkgs = { url = "gitlab:myrealm/packages/main"; };
  };

  outputs = { self, storage, nixpkgs, ... }@inputs:
  {


    nixosModules =
    #let
    #  connectivity = storage.nixosModules.connectivity;
    #  networks = storage.nixosModules.networks;
    #  machines = storage.nixosModules.machines;
    #in
    {
      nanomq = import ./modules/nanomq.nix;
      phppgadmin = import ./modules/phppgadmin.nix;
      meta-wireguard = import ./networks/meta-wireguard.nix;
      meta-dhcpd = import ./networks/meta-dhcpd.nix;
      meta-network = import ./networks/meta-network.nix;
      meta-unbound = import ./networks/meta-unbound.nix;
      meta-wg-server = import ./networks/meta-wg-server.nix;
      default = { config, lib, nixpkgs, ... }:
      with lib;
      let
        cfg = config.myrealm.layer;
      in
      {
        imports = [
          ./default.nix
        ];

        options.myrealm.layer = with types; {
          enable = mkEnableOption "enable layers";

          #domain = lib.mkOption {
          #  type = lib.types.attrsOf ( lib.types.submoduleWith  {
          #    modules = [
          #      ./domains
          #    ];
          #  });
          #};

          users = mkOption {
            type =  bool;
            default = true;
          };

          machine = mkOption {
            type =  str;
          };

          paths = mkOption {
            type = nullOr (listOf (listOf str));
            default = null;
          };
        };

        config = mkIf cfg.enable {
          environment.etc."myrealm.domain".text = builtins.toJSON cfg.domain;

          systemd.services.layer-greeter = {
            wantedBy = [ "multi-user.target" ];
            serviceConfig.Type = "oneshot";

            script = ''
              echo "#########################################"
              echo "   ### Your active layers are:"
              echo "#########################################"
              echo "   ### users: ${escapeShellArg cfg.users}"
              echo "   ### machine: ${escapeShellArg cfg.machine}"
              echo "   ### paths: ${escapeShellArg cfg.paths}"
              echo "   ### domain:"
              cat /etc/myrealm.domain
              echo "#########################################"
            '';
          };
        };
      };
    };

    #templates = {
    #  layers = {
    #    path = ./.;
    #    description = "All of layers";
    #  };
    #  user = {
    #    path = ./templates/user;
    #    description = "new user";
    #  };
    #  network = {
    #    path = ./templates/network;
    #    description = "new network";
    #  };
    #  device = {
    #    path = ./templates/device;
    #    description = "new device";
    #  };
    #  domain = {
    #    path = ./templates/domain;
    #    description = "new domain";
    #  };
    #};
  };
}
