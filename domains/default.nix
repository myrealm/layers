{ config, lib, pkgs, machines, ... }:
with lib;
let
  cfg = config.myrealm.layer.domain;
in
{
  imports = [
    ./ls
  ];

  #options.myrealm.layer.domain = lib.mkOption {
  #  #type = lib.types.attrsOf ( lib.types.submodule (import ./ls { inherit config lib pkgs machines; }));
  #  #type = lib.types.attrsOf ( lib.types.submoduleWith  {
  #  type = lib.types.submoduleWith  {
  #    modules = [
  #      ./ls
  #    ];
  #  };
  #  #});

  #  default = {};
  #};
}
