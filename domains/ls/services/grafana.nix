{ config, pkgs, lib, ... }:
let
  cfg = config.myrealm.layer.domain.ls.service.grafana;
in
{

  options.myrealm.layer.domain.ls.service.grafana = {
    enable = lib.mkEnableOption "enable grafana";

    #name = lib.mkOption {
    #  type = lib.types.str;
    #};
    ip = lib.mkOption {
      type = lib.types.str;
      default = "0.0.0.0";
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = 3000;
    };
    mutDir = lib.mkOption {
      type = lib.types.path;
      default = config.myrealm.layer.domain.ls.storage.mut;
    };

    prom = lib.mkOption {
      type = lib.types.submodule {
        options = {
          ip = lib.mkOption {
            type = lib.types.str;
            default = "localhost";
          };
          port = lib.mkOption {
            type = lib.types.port;
            default = 9090;
          };
        };
      };
    };

    loki = lib.mkOption {
      type = lib.types.submodule {
        options = {
          ip = lib.mkOption {
            type = lib.types.str;
            default = "localhost";
          };
          port = lib.mkOption {
            type = lib.types.port;
            default = 3100;
          };
        };
      };
    };
  };


  config = let
    grafDir = "${cfg.mutDir}/grafana";
    dasDir = "${cfg.mutDir}/grafana/dashboards";
  in
  lib.mkIf cfg.enable
  {
    systemd.tmpfiles.rules = [
      "d ${grafDir} 770 grafana grafana"
      "d ${dasDir} 770 grafana grafana"
    ];

    networking.firewall.allowedTCPPorts = [ cfg.port ];

    services.grafana = {
      enable = true;

      dataDir = grafDir;

      settings.server = {
        http_addr = cfg.ip;
        http_port = cfg.port;
        #addr = cfg.name;
      };

      declarativePlugins = with pkgs.grafanaPlugins; [
        grafana-piechart-panel
      ];

      settings.database = {
        host = "localhost";
        type = "postgres";
        name = "grafana";
        user = "grafana";
        password = "grafana";
      };

      provision = {
        enable = true;

        datasources.settings.datasources = [
          {
            name = "Local Prometheus";
            type = "prometheus";
            isDefault = true;
            access = "proxy";
            url = "http://${cfg.prom.ip}:${toString cfg.prom.port}";
          }
          {
            name = "Local Loki";
            type = "loki";
            access = "proxy";
            url = "http://${cfg.loki.ip}:${toString cfg.loki.port}";
          }
        ];

        dashboards.settings = {
          providers = [{
            name = "default";
            options.path = dasDir;
          }];
        };

      };
    };
  };
}
