{ config, pkgs, lib, ... }:
let
  cfg = config.myrealm.layer.domain.ls.service.prometheus;
in
{

  options.myrealm.layer.domain.ls.service.prometheus = {
    enable = lib.mkEnableOption "enable prometheus";

    ip = lib.mkOption {
      type = lib.types.str;
      default = config.services.prometheus.listenAddress;
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = 9090;
    };
    mutDir = lib.mkOption {
      type = lib.types.path;
      default = config.myrealm.layer.domain.ls.storage.mut;
    };
  };


  config = let
    promDir = "${cfg.mutDir}/prometheus";
  in
  lib.mkIf cfg.enable
  {

    networking.firewall.allowedTCPPorts = [ cfg.port ];

    systemd.tmpfiles.rules = [
      "d ${promDir} 770 prometheus prometheus"
    ];

    services.prometheus = {
      enable = true;
      listenAddress = cfg.ip;
      #port = cfg.port;
      stateDir = "prometheus-data";

      scrapeConfigs = [
        {
          job_name = "proxy";
          static_configs = [{
            targets = [
              "10.100.0.1:${toString config.services.prometheus.exporters.node.port}"
              "10.100.0.1:${toString config.services.prometheus.exporters.wireguard.port}"
              "10.100.0.1:${toString config.services.prometheus.exporters.postfix.port}"
              "10.100.0.1:${toString config.services.prometheus.exporters.nginx.port}"
            ];
          }];
        }
        {
          job_name = "adelskronen";
          static_configs = [{
            targets = [
              "10.100.0.2:${toString config.services.prometheus.exporters.node.port}"
            ];
          }];
        }
        {
          job_name = "nanopi";
          static_configs = [{
            targets = [
              "10.100.0.3:${toString config.services.prometheus.exporters.node.port}"
              "10.100.0.3:${toString config.services.prometheus.exporters.unbound.port}"
              "10.100.0.3:${toString config.services.prometheus.exporters.postgres.port}"
            ];
          }];
        }
      ];

    };
  };
}
