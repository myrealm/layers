{
  imports = [
    ./frontpage.nix
    ./loki.nix
    ./postgres.nix
    ./prometheus.nix
    ./grafana.nix
  ];
}
