{ config, pkgs, lib, ... }:
let
  cfg = config.myrealm.layer.domain.ls.service.postgres;
in
{
  options.myrealm.layer.domain.ls.service.postgres = {
    enable = lib.mkEnableOption "enable postgres";

    #ip = lib.mkOption {
    #  type = lib.types.str;
    #};
    #port = lib.mkOption {
    #  type = lib.types.port;
    #};
    metrics = lib.mkOption {
      type = lib.types.submodule {
        options = {
          ip = lib.mkOption {
            type = lib.types.str;
            default = "127.0.0.1";
          };
          port = lib.mkOption {
              type = lib.types.port;
              default = 9187;
          };
        };
      };
    };
    mutDir = lib.mkOption {
      type = lib.types.path;
      default = config.myrealm.layer.domain.ls.storage.mut;
    };
  };

  config = let
    pgDir = "${cfg.mutDir}/postgresql";
  in
  lib.mkIf cfg.enable
  {
    users.users.postgres.initialPassword = "postgres";

    systemd.tmpfiles.rules = [
      "d ${pgDir} 750 postgres postgres"
    ];

    services.prometheus.exporters.postgres = {
      enable = true;
      listenAddress = cfg.metrics.ip;
      port = cfg.metrics.port;
      openFirewall = true;
      runAsLocalSuperUser = true; #FIXME
    };
    #networking.firewall.allowedTCPPorts = [ cfg.port ];

    services.postgresql = {
      enable = true;
      #addr = cfg.ip;
      #port = cfg.port;
      package = pkgs.postgresql_14;
      dataDir = pgDir;

      enableTCPIP = true;
      authentication = lib.mkForce ''
        local all all              peer
        host  all all 127.0.0.1/32 md5
        host  all all ::1/128      md5
        host  all all 0.0.0.0/0    md5
        host  all all ::/0         md5
      '';

      extraPlugins = [
        pkgs.postgresql14Packages.timescaledb
        pkgs.postgresql14Packages.postgis
      ];

      settings = {
        shared_preload_libraries = "timescaledb";
      };

      # TODO howto password/secret management for production
      initialScript = pkgs.writeText "backend-initScript" ''

        CREATE DATABASE grafana;
        CREATE DATABASE gilgamesh;
        CREATE DATABASE geodata;


        CREATE ROLE grafana WITH LOGIN PASSWORD 'grafana' CREATEDB;
        GRANT ALL PRIVILEGES ON DATABASE grafana TO grafana;

        CREATE ROLE gilgamesh WITH LOGIN PASSWORD 'gilgamesh' CREATEDB;
        GRANT ALL PRIVILEGES ON DATABASE gilgamesh TO gilgamesh;

        CREATE ROLE geodata WITH LOGIN PASSWORD 'geodata' CREATEDB;
        GRANT ALL PRIVILEGES ON DATABASE geodata TO geodata;


        \connect gilgamesh postgres
        CREATE EXTENSION timescaledb;

        \connect geodata postgres
        CREATE EXTENSION postgis;
        CREATE EXTENSION postgis_raster;
        CREATE EXTENSION postgis_topology;
        CREATE EXTENSION postgis_sfcgal;
        CREATE EXTENSION fuzzystrmatch;
        CREATE EXTENSION address_standardizer;
        CREATE EXTENSION address_standardizer_data_us;
        CREATE EXTENSION postgis_tiger_geocoder;
      '';

    };
  };
}
