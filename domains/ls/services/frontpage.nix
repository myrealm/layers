{ config, pkgs, lib, machines, ... }:
let

  wwwDir = "/www/frontpage";
  user = "frontpage";

  cfg = config.myrealm.layer.domain.ls.service.frontpage;
in
{

  options.myrealm.layer.domain.ls.service.frontpage = {
    enable = lib.mkEnableOption "enable frontpage nginx vhost";

    ip = lib.mkOption {
      type = lib.types.nullOr lib.types.str;
      default = "127.0.0.1";
    };
  };


  config = lib.mkIf cfg.enable {

    users.users."${user}" = {
      group = "${user}";
      isSystemUser = true;
    };

    users.groups.${user}.members = [ "${user}" config.services.nginx.user ];

    networking.firewall.allowedTCPPorts = [ 80 ];

    services.nginx = {
      enable = true;

      virtualHosts."portal.living-systems.dev" = {

        root = wwwDir;

        listen = [ { addr = cfg.ip; port = 80; } ];

        locations = {
          "/" = {
              tryFiles = "$uri /index.php";
              #index index.php index.html index.htm;
              #try_files $uri $uri/ =404;
          };
          "~ \.php$" = {
            tryFiles = "$uri =404";
            extraConfig = ''
              fastcgi_split_path_info ^(.+\.php)(/.+)$;
              fastcgi_pass unix:${config.services.phpfpm.pools.${user}.socket};
              fastcgi_index index.php;
              fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
              include ${pkgs.nginx}/conf/fastcgi_params;
              include ${pkgs.nginx}/conf/fastcgi.conf;
            '';
          };
        };
      };

      virtualHosts."np.lan" = {

        root = wwwDir;

        listen = [ { addr = cfg.ip; port = 80; } ];

        locations = {
          "/" = {
              tryFiles = "$uri /index.php";
          };
          "~ \.php$" = {
            tryFiles = "$uri =404";
            extraConfig = ''
              fastcgi_split_path_info ^(.+\.php)(/.+)$;
              fastcgi_pass unix:${config.services.phpfpm.pools.${user}.socket};
              fastcgi_index index.php;
              fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
              include ${pkgs.nginx}/conf/fastcgi_params;
              include ${pkgs.nginx}/conf/fastcgi.conf;
            '';
          };
        };
      };

      virtualHosts."metrics" = {
        listen = [ { addr = "0.0.0.0"; port = 9102; } ];

        locations = {
          "/metrics" = {
            extraConfig = ''
              stub_status on;
            '';
          };
        };

      };
    };

    services.phpfpm.pools.${user} = {
      user = "${user}";
      group = "${user}";
      phpPackage = pkgs.php;

      settings = {
        "listen.owner" = config.services.nginx.user;
        "pm" = "dynamic";
        "pm.max_children" = 16;
        "pm.max_requests" = 100;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 4;
        "php_admin_value[error_log]" = "stderr";
        "php_admin_flag[log_errors]" = true;
        "catch_workers_output" = true;
      };

      phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
    };
  };
}
