{ config, pkgs, lib, ... }:
let
  cfg = config.myrealm.layer.domain.ls.service.loki;
in
{

  options.myrealm.layer.domain.ls.service.loki = {
    enable = lib.mkEnableOption "enable loki";

    ip = lib.mkOption {
      type = lib.types.str;
      default = "127.0.0.1";
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = 3100;
    };
    mutDir = lib.mkOption {
      type = lib.types.path;
      default = config.myrealm.layer.domain.ls.storage.mut;
    };
  };


  config = let
    lokDir = "${cfg.mutDir}/loki";
  in
  lib.mkIf cfg.enable
  {

    systemd.tmpfiles.rules = [
      "d ${lokDir} 770 loki loki"
    ];

    #needed?
    networking.firewall.allowedTCPPorts = [ cfg.port ];

    environment.etc."loki.json".text = ''
      auth_enabled: false

      server:
        http_listen_address: ${cfg.ip}
        http_listen_port: ${builtins.toString cfg.port}

      ingester:
        lifecycler:
          address: 127.0.0.1
          ring:
            kvstore:
              store: inmemory
            replication_factor: 1
          final_sleep: 0s
        chunk_idle_period: 5m
        chunk_retain_period: 30s

      schema_config:
        configs:
        - from: 2020-05-15
          store: boltdb
          object_store: filesystem
          schema: v11
          index:
            prefix: index_
            period: 168h

      storage_config:
        boltdb:
          directory: ${lokDir}/index

        filesystem:
          directory: ${lokDir}/chunks

      limits_config:
        enforce_metric_name: false
        reject_old_samples: true
        reject_old_samples_max_age: 168h
        split_queries_by_interval: 24h

      frontend:
        max_outstanding_per_tenant: 2048
    '';

    services.loki = {
      enable = true;
      dataDir = lokDir;
      configFile = "/etc/loki.json";
    };
  };
}
