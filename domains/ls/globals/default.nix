{
  imports = [
    ./promtail.nix
    ./node-exporter.nix
    ./build-host.nix
  ];
}
