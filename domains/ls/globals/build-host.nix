{ config, pkgs, lib, ... }:
let
  #pubkey = import ../../../users/pubkeys.nix;
  cfg = config.myrealm.layer.domain.ls.globals.build-host;
in
{

  options.myrealm.layer.domain.ls.globals.build-host = {
    enable = lib.mkEnableOption "enable adelskronen as distributed builder and binary cache";
  };


  config = lib.mkIf cfg.enable {
    # use adelskronen as binary cache
    nix.settings.substituters = [ "http://adelskronen.lan" ];
    nix.settings.trusted-public-keys = [ "adelskronen.lan:cnItGgqOm2Bg8AznUlnV7JjE45z5cn8KvOL0uY6kQtU=" ];
    #nix.binaryCachePublicKeys = [ "${pubkey.leo_bin_cache}" ];

    # use adelskronen as distributed builder
    nix.distributedBuilds = true;
    nix.buildMachines = [ {
      hostName = "adelskronen.lan";
      systems = [ "x86_64-linux" "aarch64-linux"];
      sshUser = "root";
      sshKey = "/root/.ssh/id_ed25519";
      maxJobs = 16;
      supportedFeatures = [ "kvm" "big-parallel" ];
    }];
  };
}
