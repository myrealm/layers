{ config, pkgs, lib, ... }:
let
  cfg = config.myrealm.layer.domain.ls.globals.promtail;
in
{

  options.myrealm.layer.domain.ls.globals.promtail = {
    enable = lib.mkEnableOption "enable promtail for syslog";
    ip = lib.mkOption {
      type = lib.types.str;
      default = "127.0.0.1";
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = 3100;
    };
    scrapeConfigs = lib.mkOption {
      type = lib.types.listOf lib.types.str;
      default = [ "" ];
    };
  };


  config = lib.mkIf cfg.enable {

    environment.etc."promtail.json".text = ''
      server:
        disable: true

      positions:
        filename: /tmp/positions.yaml

      clients:
        - url: http://${cfg.ip}:${builtins.toString cfg.port}/loki/api/v1/push

      scrape_configs:
        - job_name: journal
          journal:
            max_age: 12h
            labels:
              job: systemd-journal
          relabel_configs:
            - source_labels: ['__journal__systemd_unit']
              target_label: 'unit'
            - source_labels: ['__journal__hostname']
              target_label: 'hostname'
            - source_labels: ['__journal__transport']
              target_label: 'transport'
            - source_labels: ['__journal__syslog_identifier']
              target_label: 'syslog_identifier'
        ${builtins.concatStringsSep "\n" cfg.scrapeConfigs}
    '';

    systemd.services.promtail = {
      description = "Promtail service for Loki";
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        ExecStart = ''
          ${pkgs.grafana-loki}/bin/promtail --config.file /etc/promtail.json
        '';
      };
    };
  };
}
