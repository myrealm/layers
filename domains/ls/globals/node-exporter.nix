{ config, pkgs, lib, ... }:
let
  #pubkey = import ../../../users/pubkeys.nix;
  port = 9100;
  cfg = config.myrealm.layer.domain.ls.globals.node-exporter;
in
{

  options.myrealm.layer.domain.ls.globals.node-exporter = {
    enable = lib.mkEnableOption "enable adelskronen as distributed builder and binary cache";
    ip = lib.mkOption {
      type = lib.types.str;
      default = "0.0.0.0";
    };
    port = lib.mkOption {
      type = lib.types.port;
      default = config.services.prometheus.exporters.node.port;
    };
  };

  config = lib.mkIf cfg.enable {
    services.prometheus.exporters.node = {
      enable = true;
      listenAddress = cfg.ip;
      port = cfg.port;
      openFirewall = true;
    };
  };
}
