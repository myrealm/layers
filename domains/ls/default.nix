{ config, pkgs, lib, machines, ... }:
with lib;
let
  cfg = config.myrealm.layer.domain.ls;
in
{
  imports = [
    ./services
    ./globals
  ];

  options.myrealm.layer.domain.ls =  with types; {
    enable = mkEnableOption "enable domain ls with its associated defaults";

    storage = mkOption {
      type = submodule {

        options = {
          mut = mkOption {
            type = path;
          };

          www = mkOption {
            type = path;
          };

          cert = mkOption {
            type = path;
          };

          secret = mkOption {
            type = path;
          };
        };
      };
    };

  };

  config =
  let
    publicNetPath = ["ls" "k21"];
    privateNetPath = ["ls" "wg"];
    m = machines."${config.myrealm.layer.machine}";
    MAP = path: attrByPath path null m;

    #l = layers.ls;
    #LAP = path: getAttrFromPath path l;

    machine = config.myrealm.layer.machine;

    backend = machines.nanopi.ls.wg.ip;
    k21 = machines.nanopi.ls.k21.ip;

    l.service = {
      loki.host = "nanopi";
      loki.ip = backend;
      loki.port = 3100;

      postgres.host = "nanopi";
      postgres.metrics.ip = backend;
      postgres.metrics.port = 9187;

      prometheus.host = "nanopi";
      prometheus.ip = backend;
      prometheus.port = 9090;

      grafana.host = "nanopi";
      grafana.ip = k21;
      grafana.port = 3000;

      frontpage.host = "nanopi";
      frontpage.ip = k21;
      frontpage.port = 80;
    };

    l.metrics = {
      promtail = {
        ip = l.service.loki.ip;
        port =  l.service.loki.port;
      };

      node-exporter = {
        port =  9100;
        ip = MAP (privateNetPath ++ ["ip"]);
      };
    };
  in
  mkIf cfg.enable
  {

    myrealm.layer.domain.ls = {
      service = {

        loki = {
          enable = mkIf (machine == l.service.loki.host) true;
          ip = l.service.loki.ip;
          port = l.service.loki.port;
        };

        postgres = {
          enable = mkIf (machine == l.service.postgres.host) true;
          metrics.ip = l.service.postgres.metrics.ip;
          metrics.port = l.service.postgres.metrics.port;
        };

        prometheus = mkIf (machine == l.service.prometheus.host) {
          enable = true;
          ip = l.service.prometheus.ip;
          port = l.service.prometheus.port;
        };

        grafana = {
          enable = mkIf (machine == l.service.grafana.host) true;
          ip = l.service.grafana.ip;
          port = l.service.grafana.port;
          prom = {
            ip = l.service.prometheus.ip;
            port = l.service.prometheus.port;
          };
          loki = {
            ip = l.service.loki.ip;
            port = l.service.loki.port;
          };
        };

        frontpage = {
          enable = mkIf (machine == l.service.frontpage.host) true;
          ip = MAP (privateNetPath ++ ["ip"]);
        };
      };

      globals = {
        promtail = {
          enable = false;
          ip = l.service.loki.ip;
          port =  l.service.loki.port;
        };

        node-exporter = {
          enable = false;
          ip = l.metrics.node-exporter.ip;
          port = l.metrics.node-exporter.port;
        };

      #  postgres = {
      #    port = 5555;
      #  };

      #  unbound = {
      #    port = 5555;
      #  };
      };
    };
  };
}
