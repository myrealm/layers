{ config
, lib
, pkgs
, networks
, machines
, connectivity
, ... }:
with lib;

let
  cfg = config.services.meta-unbound;

  n = networks;
  m = machines;
  c = connectivity;

  cidrList = unique (forEach cfg.nets (netname:
    n.${cfg.site}.${netname}.cidr
  ));

  domainList = unique (forEach cfg.nets (netname:
    n.${cfg.site}.${netname}.domain
  ));

  allowedAccess = forEach cidrList (cidr:
    ''"${cidr}" allow''
  );

  localZone = forEach domainList (netname:
    ''"${netname}" static''
  );

  localData = flatten (mapAttrsToList (name: value:
    remove null (forEach cfg.nets (netname:
      let
        domainName = n.${cfg.site}.${netname}.domain;
      in
      if (hasAttrByPath [ "${cfg.site}" "${netname}" ] value) then
        let
          hostName = value.${cfg.site}.${netname}.name;
          ip = value.${cfg.site}.${netname}.ip;
        in
          ''"${hostName}.${domainName} IN A ${ip}"''
      else null
    ))
  ) m);

  testfmt = pkgs.formats.json {};
in
{
  ### Interface
  options = {
    services.meta-unbound = {

      enable = mkEnableOption "meta-unbound";

      site = mkOption {
        type = types.str;
        description = "Network site defined in networks.nix";
      };

      nets = mkOption {
        type = types.listOf types.str;
        description = "Network names defined in networks.nix";
      };

      interfaces = mkOption {
        type = types.listOf types.str;
        description = "Listen Interfaces";
      };

      adblock = mkOption {
        description = "Configure adblocking zones";
        default = {
          enable = true;
          package = pkgs.myrealm.unbound-adblock;
        };
        type = with types; submodule {
          options = {
            enable = mkOption {
              type = bool;
              default = true;
              description = "Whether to enable adblocking zones";
            };
            package = mkOption {
              type = package;
              default = pkgs.myrealm.unbound-adblock;
              description = ''Which package to use for adblocking zones
                this package is expected to create a symlink to /etc/unbound-adblock.cfg
              '';
            };
          };
        };
      };

      forwardAddresses = mkOption {
        type = types.listOf types.str;
        description = "Forward Addresses";
        default = [ # DNS.WATCH
          "1.1.1.1"
          "1.0.0.1"
          "2606:4700:4700::1111"
          "2606:4700:4700::1001"
        ];
      };
    };
  };

  ### Implementation
  config =
  let
    socketPath = "/run/unbound/unbound.socket";
  in
  mkIf cfg.enable {

    services.prometheus.exporters.unbound = {
      enable = true;
      openFirewall = true;
      fetchType = "uds";
      extraFlags = [ "--control-interface ${socketPath}" ];
      user = config.services.unbound.user;
      group = config.services.unbound.group;
    };

    services.unbound = {
      enable = true;
      localControlSocketPath = socketPath;
      enableRootTrustAnchor = false;
      #resolveLocalQueries = true;
      settings = {
        server = {
          interface = cfg.interfaces;
          #interface = [
          #  "127.0.0.1"
          #  "::1"
          #] ++ cfg.interfaces;
          access-control = allowedAccess;
          private-address = cidrList;
          private-domain = domainList;
          domain-insecure = domainList;
          local-zone = localZone;
          local-data = localData;
          include = [
            (if cfg.adblock.enable then "/etc/unbound-adblock.cfg" else "")
          ];
        };
        forward-zone = [
          {
            name = ".";
            forward-addr = cfg.forwardAddresses;
          }
        ];
      };
    };
    #  extraConfig = ''
    #    # enableRootTrustAnchor enables DNSSEC
    #    # DNSSEC hardening
    #    # harden-dnssec-stripped: yes

    #    # Cache elements are prefetched before they expire to keep the cache up to date.
    #    prefetch: yes
    #    cache-min-ttl: 3600
    #    cache-max-ttl: 86400

    #    # Logging (default is no).
    #    # Uncomment this section if you want to enable logging.
    #    # Note enabling logging makes the server (significantly) slower.
    #    # verbosity: 2
    #    # log-queries: yes
    #    # log-replies: yes
    #    # log-tag-queryreply: yes
    #    # log-local-actions: yes

    #    hide-identity: yes
    #    hide-version: yes

    #    # Our LAN segments.
    #    ${privateAddresses}

    #    # Private domains
    #    ${privateDomains}
    #    unblock-lan-zones: yes
    #    insecure-lan-zones: yes

    #    # Disable DNSSEC for local domains
    #    ${domainInsecure}

    #    # Local Zones
    #    ${localZones}

    #    # Adblock Zone
    #    ${if cfg.adblock.enable then "include: /etc/unbound-adblock.cfg" else ""}
    #  '';
    #};

    environment.etc = mkIf cfg.adblock.enable {
      "unbound-adblock.cfg".source = cfg.adblock.package.outPath + "/unbound-adblock.cfg";
    };
    #environment.etc."nixos/network/test.json".source = let
    #  out = { "l" = localZone; "c" = cfg; };
    #  in
    #  testfmt.generate "zwarbl" out;

    # FIXME!!
    #networking.firewall.allowedTCPPorts = [ pga_port ];
  };
}
