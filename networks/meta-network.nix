{ config, lib, pkgs, machines, networks, connectivity, ... }:

with lib;

let
  cfg = config.services.meta-network;

  #n = import ../network/networks.nix;
  #m = import ../network/machines.nix;
  #c = import ../network/connectivity.nix;
  n = networks;
  m = machines;
  c = connectivity;

  format = pkgs.formats.json {};
in
{
  ### Interface
  options = {
    services.meta-network = {

      enable = mkEnableOption "meta-network";

    };
  };

  ### Implementation
  config = mkIf cfg.enable {
    environment.etc."network.json".source = let
      out = { "n" = n; "m" = m; "c" = c; };
      in
      format.generate "zwarbl" out;
  };
}
