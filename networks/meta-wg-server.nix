{ config, lib, pkgs, machines, networks, connectivity, ... }:

with lib;

let
  cfg = config.services.meta-wg-server;

  #n = import ../network/networks.nix;
  #m = import ../network/machines.nix;
  #c = import ../network/connectivity.nix;
  n = networks;
  m = machines;
  c = connectivity;

  format = pkgs.formats.json {};

in
{
  ### Interface
  options = {
    services.meta-wg-server = {
      enable = mkEnableOption "meta-wg-server";
      iface = mkOption {
        type =  types.str;
        description = ''
          PHY interface name
        '';
      };
      domain = mkOption {
        type =  types.str;
        description = ''
          domain/site name of networks.nix
        '';
      };
      net = mkOption {
        type =  types.str;
        description = ''
          net name in networks.nix
        '';
      };
      pkiPath = mkOption {
        type =  types.str;
        description = ''
          PKI path for peers and server
        '';
        default = "/root/secrets";
      };
    };
  };

  ### Implementation
  config = let
    wgIfaceName = "wg-${cfg.domain}-${cfg.net}";
    pkiAbsPath = "${cfg.pkiPath}/${cfg.domain}/${cfg.net}";

    net = n.${cfg.domain}.${cfg.net};
    wgPort = net.port;

    genPeers = remove null ( mapAttrsToList
      (name: value:
        if (hasAttrByPath [ "${name}" "${cfg.domain}" "${cfg.net}" ] m &&
          hasAttrByPath [ "${name}" "${cfg.domain}" "${cfg.net}" "publicKey" ] m )
        then
        let
          mnet = value.${cfg.domain}.${cfg.net};
          pskf = if (hasAttrByPath [ "pskFile" ] mnet) then mnet.pskFile else null;
        in
        { # Feel free to give a meaning full name
          # Public key of the peer (not a file path).
          publicKey = "${mnet.publicKey}";
          #allowedIPs = mnet.allowedIPs; # NOPE!
          allowedIPs = mnet.ips;
          presharedKeyFile = pskf;
        }
        else null
      ) m );
    in mkIf cfg.enable
    {
      #environment.etc."test.json".source = let out = { "n" = n; "m" = m; "c" = c; }; in format.generate "zwarbl" out;
      environment.etc."test.json".source = let out = { peers = genPeers; }; in format.generate "zwarbl" out;

      networking.nat.enable = true;
      networking.nat.externalInterface = cfg.iface;
      networking.nat.internalInterfaces = [ wgIfaceName ];
      networking.firewall = { allowedUDPPorts = [ wgPort ]; };

      networking.wireguard.interfaces = {
        # "wg0" is the network interface name. You can name the interface arbitrarily.
        "${wgIfaceName}-server" = {
          # Determines the IP address and subnet of the server's end of the tunnel interface.
          ips = net.ips;

          # The port that WireGuard listens to. Must be accessible by the client.
          listenPort = wgPort;

          # This allows the wireguard server to route your traffic to the internet and hence be like a VPN
          # For this to work you have to set the dnsserver IP of your router (or dnsserver of choice) in your clients
          #postSetup = ''
          #  ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s ${net.cidr} -o ${cfg.iface} -j MASQUERADE
          #'';

          ## This undoes the above command
          #postShutdown = ''
          #  ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s ${net.cidr} -o ${cfg.iface} -j MASQUERADE
          #'';

          # Path to the private key file.
          #
          # Note: The private key can also be included inline via the privateKey option,
          # but this makes the private key world-readable; thus, using privateKeyFile is
          # recommended.
          privateKeyFile = "${pkiAbsPath}/private";

          peers = genPeers;
        };
      };

      services.prometheus.exporters.wireguard = {
        enable = true;
        openFirewall = true;
        #withRemoteIp = true;
        singleSubnetPerField = false; # default: false
        #wireguardConfig = null; # FIXME "friendly names"
        #firewallFilter = "-i eth0 -p tcp -m tcp --dport 9586";
        listenAddress = "${net.ip}";
        #verbose = true;
      };
  };
}
